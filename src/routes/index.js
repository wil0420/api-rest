const { Router } = require('express');
const router = Router();


router.get('/', (req,res) =>{
    res.json({
        name : "Prueba",
        apellido: "Apellido"
    });

});

router.get('/get', (req,res) =>{
    const datos ={
        nombre: "Wilmer",
        website: "wil.com"
    }

    res.json(datos);

});

router.post('/add', (req,res) =>{
    const datos ={
        nombre: "Wilmer",
        website: "wil.com"
    }

    res.json(datos);

});



module.exports = router;